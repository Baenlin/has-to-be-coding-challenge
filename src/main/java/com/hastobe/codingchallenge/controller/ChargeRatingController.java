package com.hastobe.codingchallenge.controller;

import com.hastobe.codingchallenge.service.RateCalculationService;
import com.hastobe.codingchallenge.dto.ChargingCosts;
import com.hastobe.codingchallenge.dto.ChargeStationInput;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller responsible to receive request about calculating charging costs.
 */
@RestController
@RequestMapping(value = ChargeRatingController.ENDPOINT_URL_PATH)
public class ChargeRatingController {
    /**
     * URL path.
     */
    public final static String ENDPOINT_URL_PATH = "/";

    @Autowired
    RateCalculationService rateCalculationService;

    /**
     * Endpoint to calculate some charging costs within some passed rates and charging details.
     *
     * @param chargeStationInput includes rates and details about charging process.
     * @return charging costs.
     * @throws Exception on invalid inputs.
     */
    @PostMapping(ENDPOINT_URL_PATH + "rate")
    public ChargingCosts rate(@RequestBody final ChargeStationInput chargeStationInput) throws Exception {
        return rateCalculationService.rate(chargeStationInput);
    }
}
