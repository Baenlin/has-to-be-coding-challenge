package com.hastobe.codingchallenge.dto;

import java.math.BigDecimal;

/**
 * A rate includes values for energy, time or transaction cost.
 */
public class Rate {
    private BigDecimal energy;
    private BigDecimal time;
    private BigDecimal transaction;

    public Rate(final BigDecimal energy, final BigDecimal time, final BigDecimal transaction) {
        this.energy = energy;
        this.time = time;
        this.transaction = transaction;
    }

    /**
     * Energy costs.
     */
    public BigDecimal getEnergy() {
        return energy;
    }

    /**
     * Time costs.
     */
    public BigDecimal getTime() {
        return time;
    }

    /**
     * Transaction costs.
     */
    public BigDecimal getTransaction() {
        return transaction;
    }

    @Override
    public String toString() {
        return "Rate{" +
                "energy=" + energy +
                ", time=" + time +
                ", transaction=" + transaction +
                '}';
    }
}
