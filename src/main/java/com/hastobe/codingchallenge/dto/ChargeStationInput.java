package com.hastobe.codingchallenge.dto;

/**
 * ChargeStationInput which includes information about rates and charging process details.
 */
public class ChargeStationInput {
    private Rate rate;
    private ChargeDetailRecord cdr;

    public ChargeStationInput() {

    }

    public ChargeStationInput(final Rate rate, final ChargeDetailRecord chargeDetailRecord) {
        this.rate = rate;
        this.cdr = chargeDetailRecord;
    }

    /**
     * Includes components like energy, time or transaction cost.
     */
    public Rate getRate() {
        return rate;
    }

    /**
     * Charge Detail Record short 'CDR' includes information about an actual charging process.
     */
    public ChargeDetailRecord getCdr() {
        return cdr;
    }

    @Override
    public String toString() {
        return "ChargeStationInput{" +
                "rate=" + rate +
                ", chargeDetailRecord=" + cdr +
                '}';
    }
}
