package com.hastobe.codingchallenge.dto;

import java.util.Date;

/**
 * Data transfer object of data from the charging process.
 */
public class ChargeDetailRecord {
    private long meterStart;
    private long meterStop;
    private Date timestampStart;
    private Date timestampStop;

    public ChargeDetailRecord( final Date timestampStart,
                               final Date timestampStop,
                               final long meterStart,
                               final long meterStop) {
        this.meterStart = meterStart;
        this.meterStop = meterStop;
        this.timestampStart = timestampStart;
        this.timestampStop = timestampStop;
    }

    /**
     * Value of the electricity meter when the charging process was started.
     */
    public long getMeterStart() {
        return meterStart;
    }

    /**
     * Value of the electricity meter when the charging process was stopped.
     */
    public long getMeterStop() {
        return meterStop;
    }

    /**
     * Timestamp when the charging process was started.
     */
    public Date getTimestampStart() {
        return timestampStart;
    }

    /**
     * Timestamp when the charging process was stopped.
     */
    public Date getTimestampStop() {
        return timestampStop;
    }

    @Override
    public String toString() {
        return "ChargeDetailRecord{" +
                "meterStart=" + meterStart +
                ", meterStop=" + meterStop +
                ", timestampStart=" + timestampStart +
                ", timestampStop=" + timestampStop +
                '}';
    }
}
