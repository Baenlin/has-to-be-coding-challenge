package com.hastobe.codingchallenge.dto;

import java.math.BigDecimal;

/**
 * Calculated costs for some charging process.
 */
public class ChargingCosts {
    final BigDecimal time;
    final BigDecimal energy;
    final BigDecimal transaction;
    final BigDecimal overall;

    public ChargingCosts(BigDecimal overall,
                         BigDecimal energy,
                         BigDecimal time,
                         BigDecimal transaction) {
        this.overall = overall;
        this.energy = energy;
        this.time = time;
        this.transaction = transaction;
    }

    /**
     * Total amount.
     */
    public BigDecimal getOverall() {
        return this.overall;
    }

    /**
     * Energy costs.
     */
    public BigDecimal getEnergy() {
        return this.energy;
    }

    /**
     * Time costs.
     */
    public BigDecimal getTime() {
        return this.time;
    }

    /**
     * Transaction costs.
     */
    public BigDecimal getTransaction() {
        return this.transaction;
    }
}
