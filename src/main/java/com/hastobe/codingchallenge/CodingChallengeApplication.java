package com.hastobe.codingchallenge;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Application that exposes endpoint about calculate charging costs.
 */
@SpringBootApplication
public class CodingChallengeApplication {
	public static void main(String[] args) {
		SpringApplication.run(CodingChallengeApplication.class, args);
	}
}
