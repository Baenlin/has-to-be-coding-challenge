package com.hastobe.codingchallenge.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Exception for invalid inputs.
 */
@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class InvalidInputException extends RuntimeException {
    public InvalidInputException(String message) {
        super(message);
    }
}
