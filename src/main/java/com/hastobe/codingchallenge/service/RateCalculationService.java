package com.hastobe.codingchallenge.service;

import com.hastobe.codingchallenge.dto.ChargingCosts;
import com.hastobe.codingchallenge.dto.ChargeStationInput;

public interface RateCalculationService {

    /**
     * Calculate charging costs within some passed rates and charging details.
     *
     * @param chargeStationInput includes rates and details about the actual charging process.
     */
    ChargingCosts rate(final ChargeStationInput chargeStationInput) throws Exception;
}
