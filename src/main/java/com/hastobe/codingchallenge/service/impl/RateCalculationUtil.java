package com.hastobe.codingchallenge.service.impl;

import com.hastobe.codingchallenge.dto.ChargingCosts;
import com.hastobe.codingchallenge.dto.ChargeDetailRecord;
import com.hastobe.codingchallenge.dto.Rate;
import com.hastobe.codingchallenge.exceptions.InvalidInputException;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.concurrent.TimeUnit;

/**
 * Utilities for calculating charging rates on CDRs.
 */
public class RateCalculationUtil {
    /**
     * Validates input values which are needed for calculating charging costs.
     * @param rate actual rate.
     * @param chargeDetailRecord actual record
     * @throws Exception .
     */
    public static void validateInputs(final Rate rate, final ChargeDetailRecord chargeDetailRecord) throws Exception {
        final String invalidParameters = "Invalid '%s' passed for rating. Error: %s";
        if (rate == null) {
            throw new InvalidInputException(String.format(invalidParameters, "rate", "Object is null."));
        }

        if (rate.getEnergy().doubleValue() < 0.f) {
            throw new InvalidInputException(String.format(invalidParameters, "rate", "Energy cost is smaller than zero."));
        }

        if (rate.getTime().doubleValue() < 0.f) {
            throw new InvalidInputException(String.format(invalidParameters, "rate", "Time cost is smaller than zero."));
        }

        if (rate.getTransaction().doubleValue() < 0.f) {
            throw new InvalidInputException(String.format(invalidParameters, "rate", "Transaction cost is smaller than zero."));
        }

        if (chargeDetailRecord == null) {
            throw new InvalidInputException(String.format(invalidParameters, "chargeDetailRecord", "Object is null"));
        }


        if (chargeDetailRecord.getMeterStart() < 0.f || chargeDetailRecord.getMeterStart() > chargeDetailRecord.getMeterStop()) {
            throw new InvalidInputException(String.format(invalidParameters,
                    "chargeDetailRecord",
                    "Meter start can't be lower than zero or larger than meterStop."));
        }

        if (chargeDetailRecord.getMeterStop() < 0.f) {
            throw new InvalidInputException(String.format(invalidParameters,
                    "chargeDetailRecord",
                    "Meter stop can't be lower than zero."));
        }

        if (chargeDetailRecord.getTimestampStop().before(chargeDetailRecord.getTimestampStart())) {
            throw new InvalidInputException(String.format(invalidParameters,
                    "chargeDetailRecord",
                    "TimeStampStop can't be before TimeStampStart."));
        }
    }

    /**
     * Calculate some charging cost regarding input rate and information about actual charging process.
     * @param rate .
     * @param chargeDetailRecord .
     * @return charging costs.
     */
    public static ChargingCosts calculate(final Rate rate, final ChargeDetailRecord chargeDetailRecord) {
        final BigDecimal timeCost = calculateTimeCost(rate, chargeDetailRecord);
        final BigDecimal energyCost = calculateEnergyCost(rate, chargeDetailRecord);
        final BigDecimal transactionCost = calculateTransactionCost(rate);
        BigDecimal overallCost = timeCost.add(energyCost).add(transactionCost);

        // The resulting overall amount needs to be rounded to 2 decimal places.
        overallCost = overallCost.setScale(2, RoundingMode.HALF_EVEN);
        overallCost = overallCost.stripTrailingZeros();

        return new ChargingCosts(overallCost, energyCost, timeCost, transactionCost);
    }

    private static BigDecimal calculateEnergyCost(final Rate rate, final ChargeDetailRecord chargeDetailRecord) {
        final long meterSum = Math.abs(chargeDetailRecord.getMeterStop() - chargeDetailRecord.getMeterStart());
        final BigDecimal energyRate = rate.getEnergy();
        final BigDecimal notRoundedEnergyCost =
                energyRate.multiply(new BigDecimal(meterSum)).divide(new BigDecimal(1000));
        return applyComponentScaleAndStripTrailingZeros(notRoundedEnergyCost);
    }

    private static BigDecimal calculateTimeCost(final Rate rate, final ChargeDetailRecord chargeDetailRecord) {
        final long differenceInMilliseconds = Math.abs(
                chargeDetailRecord.getTimestampStop().getTime() - chargeDetailRecord.getTimestampStart().getTime());
        final float hours = TimeUnit.MINUTES.convert(differenceInMilliseconds, TimeUnit.MILLISECONDS) / 60.f;
        final BigDecimal timeRate = rate.getTime();
        return applyComponentScaleAndStripTrailingZeros(timeRate.multiply(BigDecimal.valueOf(hours)));
    }

    private static BigDecimal calculateTransactionCost(final Rate rate) {
        return applyComponentScaleAndStripTrailingZeros(rate.getTransaction());
    }

    /**
     * The component prices are calculated with a precision of 3 decimal places
     *
     * @param value .
     * @return value with adjusted precision.
     */
    private static BigDecimal applyComponentScaleAndStripTrailingZeros(final BigDecimal value) {
        return value.setScale(3, RoundingMode.HALF_EVEN).stripTrailingZeros();
    }
}
