package com.hastobe.codingchallenge.service.impl;

import com.hastobe.codingchallenge.dto.ChargeDetailRecord;
import com.hastobe.codingchallenge.dto.ChargeStationInput;
import com.hastobe.codingchallenge.dto.ChargingCosts;
import com.hastobe.codingchallenge.dto.Rate;
import com.hastobe.codingchallenge.exceptions.InvalidInputException;
import com.hastobe.codingchallenge.service.RateCalculationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class RateCalculationServiceImpl implements RateCalculationService {
    private static Logger LOG = LoggerFactory.getLogger(RateCalculationService.class.getSimpleName());

    @Override
    public ChargingCosts rate(final ChargeStationInput chargeStationInput) throws Exception {
        if (chargeStationInput == null) {
            throw new InvalidInputException("Invalid payload passed to rate calculation.");
        }

        final Rate rate = chargeStationInput.getRate();
        final ChargeDetailRecord chargeDetailRecord = chargeStationInput.getCdr();
        RateCalculationUtil.validateInputs(rate, chargeDetailRecord);

        LOG.trace("Start calculate charge detail record '{}' with rate '{}'.",
                chargeDetailRecord.toString(),
                rate.toString());

        return RateCalculationUtil.calculate(rate, chargeDetailRecord);
    }
}
