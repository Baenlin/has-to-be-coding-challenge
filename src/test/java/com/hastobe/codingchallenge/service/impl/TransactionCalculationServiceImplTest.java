package com.hastobe.codingchallenge.service.impl;

import com.hastobe.codingchallenge.dto.ChargeDetailRecord;
import com.hastobe.codingchallenge.dto.ChargeStationInput;
import com.hastobe.codingchallenge.dto.ChargingCosts;
import com.hastobe.codingchallenge.dto.Rate;
import com.hastobe.codingchallenge.service.RateCalculationService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.math.BigDecimal;
import java.util.Date;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class TransactionCalculationServiceImplTest {
    private static final Rate SIMPLE_RATE = new Rate(BigDecimal.TEN , BigDecimal.ONE, BigDecimal.valueOf(0.5f));
    private static final Date START_TIMESTAMP = new Date(1617609840000L);
    private static final Date STOP_TIMESTAMP = new Date(1617614820000L);
    private static final long START_METER = 1204307L;
    private static final long STOP_METER = 1215230L;

    private RateCalculationService rateCalculationService;

    @BeforeEach
    void setUp() {
        rateCalculationService = new RateCalculationServiceImpl();
    }

    @Test
    void testRateCalculation_NullInput() {
        assertThrows(Exception.class, () -> rateCalculationService.rate(null));
    }

    @ParameterizedTest
    @MethodSource("invalidRateCalculationParameters")
    void testRateCalculation_InvalidInputs(final ChargeStationInput chargeStationInput) {
        assertThrows(Exception.class, () -> rateCalculationService.rate(chargeStationInput));
    }

    private static Stream<ChargeStationInput> invalidRateCalculationParameters() {
        return Stream.of(
                new ChargeStationInput(null, null),
                // Invalid rate values
                new ChargeStationInput(new Rate(BigDecimal.valueOf(-1), BigDecimal.ONE, BigDecimal.ONE), null),
                new ChargeStationInput(new Rate(BigDecimal.ONE, BigDecimal.valueOf(-1), BigDecimal.ONE), null),
                new ChargeStationInput(new Rate(BigDecimal.ONE, BigDecimal.ONE, BigDecimal.valueOf(-1)), null),
                // Invalid timestamp
                createChargeStationInput(SIMPLE_RATE, STOP_TIMESTAMP, START_TIMESTAMP, 0, 1),
                // Invalid meter
                createChargeStationInput(SIMPLE_RATE, START_TIMESTAMP, STOP_TIMESTAMP, 1, 0),
                createChargeStationInput(SIMPLE_RATE, START_TIMESTAMP, STOP_TIMESTAMP, -1, 0));
    }

    @ParameterizedTest
    @MethodSource("rateCalculationParameters")
    void testRateCalculation(
            final ChargeStationInput chargeStationInput,
            final ChargingCosts expectedResult) throws Exception {
        final ChargingCosts actualResult = rateCalculationService.rate(chargeStationInput);
        assertEquals(expectedResult.getEnergy(), actualResult.getEnergy());
        assertEquals(expectedResult.getTime(), actualResult.getTime());
        assertEquals(expectedResult.getTransaction(), actualResult.getTransaction());
        assertEquals(expectedResult.getOverall(), actualResult.getOverall());
    }

    private static Stream<Arguments> rateCalculationParameters() {
        return Stream.of(
                // Example from PDF:
                Arguments.of(
                        createChargeStationInput(new Rate(
                                    BigDecimal.valueOf(0.3f),
                                    BigDecimal.valueOf(2L),
                                    BigDecimal.valueOf(1L)),
                                START_TIMESTAMP,
                                STOP_TIMESTAMP,
                                START_METER,
                                STOP_METER),
                        new ChargingCosts(
                                BigDecimal.valueOf(7.04),
                                BigDecimal.valueOf(3.277),
                                BigDecimal.valueOf(2.767),
                                BigDecimal.valueOf(1))),
                // Zero transaction and time costs
                Arguments.of(createChargeStationInput(new Rate(
                                        BigDecimal.valueOf(0.5f),
                                        BigDecimal.valueOf(0L),
                                        BigDecimal.valueOf(0L)),
                                START_TIMESTAMP,
                                STOP_TIMESTAMP,
                                START_METER,
                                STOP_METER),
                        new ChargingCosts(
                                BigDecimal.valueOf(5.46),
                                BigDecimal.valueOf(5.462),
                                BigDecimal.valueOf(0),
                                BigDecimal.valueOf(0))),
                // No costs at all
                Arguments.of(createChargeStationInput(new Rate(
                                BigDecimal.valueOf(0L),
                                BigDecimal.valueOf(0L),
                                BigDecimal.valueOf(0L)),
                        START_TIMESTAMP,
                        STOP_TIMESTAMP,
                        START_METER,
                        STOP_METER),
                        new ChargingCosts(
                                BigDecimal.valueOf(0),
                                BigDecimal.valueOf(0),
                                BigDecimal.valueOf(0),
                                BigDecimal.valueOf(0)))
        );
    }

    private static ChargeStationInput createChargeStationInput(
            final Rate rate,
            final Date startTimestamp,
            final Date stopTimestamp,
            final long startMeter,
            final long stopMeter) {
        final ChargeDetailRecord chargeDetailRecord = new ChargeDetailRecord(
                startTimestamp,
                stopTimestamp,
                startMeter,
                stopMeter);
        return new ChargeStationInput(rate, chargeDetailRecord);
    }
}
