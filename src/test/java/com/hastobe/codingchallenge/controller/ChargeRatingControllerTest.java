package com.hastobe.codingchallenge.controller;

import com.hastobe.codingchallenge.CodingChallengeApplication;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockServletContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.servlet.ServletContext;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = { CodingChallengeApplication.class })
@WebAppConfiguration
public class ChargeRatingControllerTest {

    @Autowired
    private WebApplicationContext webApplicationContext;

    private MockMvc mockMvc;

    public ChargeRatingControllerTest(WebApplicationContext webApplicationContext) {
        this.webApplicationContext = webApplicationContext;
    }

    @BeforeEach
    public void setup() throws Exception {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
    }

    @Test
    public void testBeanAvailable() {
        ServletContext servletContext = webApplicationContext.getServletContext();
        Assert.assertNotNull(servletContext);
        assertTrue(servletContext instanceof MockServletContext);
        Assert.assertNotNull(webApplicationContext.getBean("chargeRatingController"));
    }

    @Test
    public void testRequestRate_NullRate() throws Exception {
        this.mockMvc.perform(
                post("/rate").contentType(APPLICATION_JSON_UTF8)
                        .content("{ \"rate\": null }"))
                .andDo(print()).andExpect(status().is5xxServerError())
                .andExpect(result -> assertTrue(
                        result.getResolvedException().getMessage().contains("Invalid 'rate' passed for rating. Error: Object is null.")));
    }

    @Test
    public void testRequestRate() throws Exception {
        this.mockMvc.perform(
                post("/rate").contentType(APPLICATION_JSON_UTF8)
                        .content("{\n" +
                                " \"rate\": { \"energy\": 0.5, \"time\": 1, \"transaction\": 5 },\n" +
                                " \"cdr\": { \"meterStart\": 1204000, \"timestampStart\": \"2022-04-18T10:00:00Z\", \"meterStop\": 1205000, \"timestampStop\":\n" +
                                "\"2022-04-18T11:00:00Z\" }\n" +
                                "}"))
                .andDo(print()).andExpect(status().isOk())
                .andExpect(result -> assertEquals("{\"time\":1,\"energy\":0.5,\"transaction\":5,\"overall\":6.5}",
                        result.getResponse().getContentAsString()));
    }
}
