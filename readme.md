# Challenge 1

My favorite tech stack for this coding challenge was using Spring Boot + Java for developing the application. I used
gradle to build the application because it's simple, and I'm used to it. For testing, I did choose simple nUnit and MVC tests.
I used for the developer environment Intellij IDEA Community Edition.

## Running the application

_A requirement is that java is installed on executing computer._

 - Execute Gradle build within command `gradlew build` or Intellij build.
 - Run attached Intellij run configuration OR ...
 - Navigate to build/lib directory in terminal and run `java -jar coding-challenge-1.0.0-SNAPSHOT.jar` on windows machine.
 
 Use for example Postman to send POST request:
 
 The following endpoint should be available after starting spring application: `localhost:8080/rate`
 Use for example the following request with a REST test tool like Postman (https://www.postman.com).
```{
  "rate": { "energy": 0.3, "time": 2, "transaction": 1 },
  "cdr": { "meterStart": 1204307, "timestampStart": "2021-04-05T10:04:00Z", "meterStop": 1215230, "timestampStop":
 "2021-04-05T11:27:00Z" }
 }
```
 
 ## Running unit tests
  - Execute Gradle test within command `gradlew test` OR...
  - Execute tests with Intellij UI.

# Challenge 2

## Improvements:
 - Rename some parameters to be more specific - just `transaction` is not very self-explanatory, I would name it
   `transactionCost` or `transactionAmount`.
 - Provide some pipeline for CI/CD to get early feedback when something is wrong with software.
 - Some additional testing UI could be helpful.
 - Maybe send start and end event with separate endpoints (not just one '/rate' endpoint). Then the Spring application could keep the `startTimestamp` and `startMeter` in database.
   
## Security Improvements:
 - Think about separating 'rates' from 'CDR' or make sure rates were received from some trusted origin.
 - Add some authentication.
